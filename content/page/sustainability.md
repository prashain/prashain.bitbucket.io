---
title: "Carbon aware software engineering"
date: 2022-12-08T04:14:00Z
categories:
- Sustainability
- Green Engineering
tags:
- Sustainability
- Green Engineering
keywords:
- tech
comments:       false
showMeta:       false
showActions:    false
#thumbnailImage: //example.com/image.jpg
---

As a sustainability advocate, I am heartened to see Software industries adopting Sustainability with vigour and enthusiasm.

I would want to take you back to 1999, when I had my first stint with Software Sustainability (back then, I was not as enlightened as now, but I had a fair idea of recycling, reusing, and reducing waste). It was the first day of the computer lab, and I was marvelling at the magic of the i486 desktop.</br>
Soon enough, I would get my desktop, a shiny Pentium 166 MHz,512 KB L2 cache. I booted my machine and spotted a **star** symbol similar to the attached image during the booting.

<table border="0">
 <tr>
    <td><b style="font-size:40%"></b><img src="/images/EnergyStar.png" alt="drawing" /> </td>
    <td><b style="font-size:60%"></b> I am sure you are familiar with <a href="https://www.energystar.gov">[Energy Star]</a>compliant systems - An initiative that started in 1992 to help manage the environmental impact of the growing computing sector. It has since increased its purview to manage energy compliance from consumer products to energy-efficient homes, buildings, industrial plants.</td>
 </tr>
 <tr>
 <td><p style="text-align: center;"><i> Exhibit 1 </i></p></td>
 <td></td>
 </tr>
</table>

Since last two-year, Sustainability has forayed into Software Engineering with foundations such as <a href="https://greensoftware.foundation">Green Software Foundation</a> lowering the threshold for industries and engineers to embrace Sustainability. Reducing the threshold is essential as we must provide engineers and managers with tools and techniques to internalise Sustainability rather than think of it as an <i>after-thought-managament-buzz-word</i>.

While lowering carbon footprint is vital, I think “Energy-aware Computing” is more apt. It will help us not only reduce our carbon footprint but also ensures we commit to reducing energy wastage, design systems that can hibernate when not in use , optimise its energy usage and many things more. In this series of blogs, I wish to set the narrative of Energy-aware Computing and give a sneak preview of a prescriptive framework, we are building towards building energy-aware systems.

Let’s revisit some of the key terms of green engineering.
1.  Total carbon footprint – Net emissions for producing and operating a system.
2.  Carbon intensity – Emissions (CO2) released to produce electricity (KwH).

A good energy aware system targets both production and operating processes while designing application/services.</br>
<img src="/images/Picture_1.png" alt="energyaware" />
<p style="text-align: center;"><i> Exhibit 2 </i></p>

Often, we just delve into creating a highly performant and energy-efficient system, which, when operational, will consume less energy but forget to give importance to the production or post-operation support aspect. While the high energy profile for production of the app will have an upward impact on the embodied energy of the system, the poor operation-support will increase the operational footprint. To put things in perspective, a typical laptop screen wattage is 65W. A simple thing such as an absent “Readme”, which provides application information to both the new engineers and the support engineers, from the source code leads to almost ~1-2 hours of information scouring journey for an engineer.

### Discovery process
To understand both factors' impact on energy usage, we collected data points such as cycle-time, injected defects, and retrospective ceremony notes in addition to the interviews with some stakeholders.
Quite unsurprisingly, the two factors that came on top were

1.  Suboptimal developer experience
2.  Dubious design decisions</br>

The inability to provide an engineer with the right toolset alone caused a significant shift in the time-to-deliver, indirectly contributing to the engineer spending more than the estimated time to deliver the user-story. Referring again to the laptop/desktop wattage metric, it becomes apparent that embodied energy is correlated to the developer experience.

We ran a second set of tests to understand the correlation between the design decisions and coding practices with the energy profile. After analysing the performance data, and profile reports, we identified a set of rules, which, when compromised in the application, leads to an increase in the energy profile. We load-tested applications that violated these "rules" with original source code and then with the remediated code, while collecting energy stats using an energy profiler.


|             | Sync Logging | Async Logging|
| ----------- | ----------- |---------------|
| Processor Energy/hr      | 3672J       | 3060J|
<p style="text-align: center;"><span><i> Exhibit3 </i></span></p>

As one can see Exhibit 3 the energy usage drops by almost 16% by using Async Logging rather than sync-logging.

---------------

The stats from the profiler demonstrated a positive correlation between the "rules" and energy profile. Some of the rules, which we identified were
1.  Asynchronous vs Sync activities
2.  Caching
3.  Compression
4.  Avoid compute heavy API's

Some of the design decisions which came on the fore were
1.  Embrace Reactive systems
2.  No rest for internal microservices (Prefer gRPC)



### Framework
Based on the useful information and deductions from phase 1, we challenged ourselves to find a solution that ensures that any greenfield project will be designed with "green-first" principle, is energy-aware and provide a quick feedback-loop on any violations of the "rules". 
We decided to build a prescriptive framework, which will help the engineers build an energy-aware appliction and contain guard-rails to ensure conformance to the rules/principles.This led to birth of a toolkit - an extensible command line application with the following features (and growing)
1.  Built on Reactive stack
2.  Uses gRPC for internal microservice.
3.  Baked-in green-rules and plugins for verifying the violation of the rules.

The "cloud-native energy-aware tookit" is a suite of applications that has best practices at its heart. At the lowest common denomination it bootstraps an engineer to rapidly build cloud-native energy-aware microservices.TEAK has been deployed within a high-street bank and initial results looks promising where the onboarding time for an engineer to build microsrvices were reduced by 30%.In addition, it made it very easy to roll out new features. TEAK is planned to create a truly cloud-native using CNCF stable technologies.


<img src="/images/teak.png" alt="tookit" />
<p style="text-align: center;"><span><i> Exhibit 4 </i></span></p>

For the fast feedback loop, we integrated the rules to both build (for maven/gradle plugins) and CI processes (SONAR).The engineers can view "green rules" violation right from thier IDE or as part of CI process. 

The next series, I will delve deeper into the SONAR integration, TEAK and SONAR rules.

---------------




